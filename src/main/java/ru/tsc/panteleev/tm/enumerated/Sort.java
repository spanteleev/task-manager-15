package ru.tsc.panteleev.tm.enumerated;

import ru.tsc.panteleev.tm.comparator.CreatedComporator;
import ru.tsc.panteleev.tm.comparator.DateBeginComporator;
import ru.tsc.panteleev.tm.comparator.NameComparator;
import ru.tsc.panteleev.tm.comparator.StatusComporator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status" , StatusComporator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComporator.INSTANCE),
    DATE_BEGIN("Sort by date begin", DateBeginComporator.INSTANCE);

    private String displayName;

    private final Comparator comparator;

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Sort sort: values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
