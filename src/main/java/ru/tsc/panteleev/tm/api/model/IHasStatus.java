package ru.tsc.panteleev.tm.api.model;

import ru.tsc.panteleev.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
