package ru.tsc.panteleev.tm.api.repository;

import ru.tsc.panteleev.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
